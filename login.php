<?php

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// Начинаем сессию.
session_start();

// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.
if (!empty($_SESSION['login']))
    // Если есть логин в сессии, то пользователь уже авторизован.
    // TODO: Сделать выход (окончание сессии вызовом session_destroy()
    //при нажатии на кнопку Выход).
    // Делаем перенаправление на форму.
    {

        session_destroy();
        if($_COOKIE['admin']=='1') {
            setcookie('admin','0');
            header('Location:admin.php');
        }
        else {
            header('Location:index.php');
        }

}

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    $flag=0;
    ?>

    <html>
    <head>
        <script src="https://kit.fontawesome.com/e2ac9cc532.js" crossorigin="anonymous"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sign in</title>
        <link  href="style.css" rel="stylesheet"  media="all"/>
    </head>
        <body>
        <?php
        if (!empty($messages)) {
            print('<div id="messages">');
            // Выводим все сообщения.
            foreach ($messages as $message) {
                print($message);
            }
            print('</div>');
        }
        ?>

            <form action="" accept-charset="UTF-8" method="POST" class="decor">
                <div class =" form-left-decoration"></div>
                <div class="form-right-decoration"></div>
                <div class="circle"></div>
                <div class="form-inner">



                        <label>

Ваш логин
                            <input class="formname" type="text"  name="login" placeholder="Введите логин"
                        </label>

                        <label>

                            Ваш пароль
                        <input class="formmail" type="password" name="pass" placeholder="Введите пароль">
                    </label>

                    <input  style="color:white;margin-left: 100px" type="submit" id="send" class="buttonform" value="Отправить">


        </div>
        </form>
    </body>
    </div>
    </html>


    <?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {
    $user = 'u24335';
    $pass = '7834647';
    $db = new PDO('mysql:host=localhost;dbname=u24335', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    $login = $_POST['login'];
    $stmt = $db->prepare("SELECT * FROM users WHERE login LIKE ?");
    $stmt->execute([$login]);
    $flag=0;
    $id='';
    while($row = $stmt->fetch())
    {
        if(!strcasecmp($row['login'],$_POST['login'])&&password_verify($_POST['pass'],$row['hash']))
        {
            $flag=1;
            $id=$row['id'];

        }
    }
    if($flag) {
        // Если все ок, то авторизуем пользователя.
        $_SESSION['login'] = $_POST['login'];
        // Записываем ID пользователя.
        $_SESSION['uid'] = $id;
        // Делаем перенаправление.
        header('Location: index.php');
    }
    else{
        header('Location: login.php');
    }
}
